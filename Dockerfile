FROM openjdk:12
ADD target/docker-spring-boot-luis_casas.jar docker-spring-boot-luis_casas.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-luis_casas.jar"]
